Extreme Rise of the Triad v1.3 - DosBox Linux Helper Scripts  
(for The Apogee Throwback Pack from Steam on Linux)  
===================================================  
  
I put this together because I tried to run The Apogee 
Throwback Pack on Linux using Proton, but the launcher
doesn't work. Then I tried running the .exe DOS files
manually with proton and the DosBox that Steam ships
along with the games, but while DosBox would open, it
would only display a blank screen. After that, I tried
installing the native Linux version of DosBox, and 
running the .exe files with that, but it turns out
Steam ships broken DosBox config files for the games, 
so that didn't work either. So I fixed it and this 
method works, at least on my Arch Linux and Debian 
systems. Maybe it will work for you too.  
  
NOTE: You may need to put the correct midi device ID 
for your system in the DosBox config files to get 
working sound. To list the midi device IDs for your 
system, you can run this command:  
```shell  
aplaymidi -l
```
  
Or for a more automated approach, you can get the first
midi device ID with this command:  
```shell
aplaymidi -l | head -n 2 | tail -n 1 | awk '{print $1}'
```
  
And the second midi device ID with this command:  
```shell
aplaymidi -l | head -n 3 | tail -n 1 | awk '{print $1}'
```
  
Then update the DosBox config files, putting the Port
number for your midi device from one of the above 
commands as the value for the "midiconfig=" option. You 
will have to run `./install.sh` again afterwards, since 
you modified some DosBox config files.  
  
===================================================  
  
  
Usage:  
------  
First, make sure you have The Apogee Throwback Pack 
from Steam installed, then run the install.sh script, 
like this:  
```shell
./install.sh
```
  
Make sure you re-run the install script any time you 
make changes to any of the DosBox config files
(the .sh files in this folder and the ipx/ folder). It 
will copy them into the correct place on your system.  
  
You can also override the default DOSBOX folder path 
by setting an environment variable, like this:  
```shell
ROTT_E_DOSBOX_PATH="/path/to/your/steam-library/steamapps/common/The Apogee Throwback Pack/DOSBOX" \
./install.sh
```
  
Now you can play Extreme Rise of the Triad without
Proton, using the native version of DosBox for your 
system!  
  

Examples:  
---------  
Game:  
```shell
./rottextreme
```
  
Help and other interesting info:  
```shell
./rottextreme-help
```
  
  
Multiplayer (IPX Network Play, a.k.a. "NETROTT"):  
-------------------------------------------------  
Run a non-dedicated server for three players. This
is the most basic type of server, where you host
and play at the same time. It supports a maximum
of 8 players:  
```shell
PLAYERS=3 SERVER_PORT="1088" ./rottextreme-ipxserver
```
  
Run a dedicated server, and also a separate
client. This allows you to host a game with
more than 8 players (unlike the non-dedicated
server), and you can also play at the same
time on the same computer that's hosting:  
```shell
PLAYERS=9 SERVER_PORT="1088" ./rottextreme-ipx
```
  
Run only the dedicated server so others can
connect and play but you won't be playing:  
```shell
PLAYERS=3 SERVER_PORT="1088" ./rottextreme-ipxdedicatedserver
```
  
Connect to a server running on the same computer 
and play:  
```shell
CLIENT_SERVER=1 SERVER_PORT="1088" ./rottextreme-ipxclient
```
  
Connect to a remote server and play:  
```shell
SERVER_ADDR="192.168.1.134" SERVER_PORT="1088" ./rottextreme-ipxclient
```
  

Utilities  
---------  
Run the Setup utility, which can be used to 
activate extra levels and change game settings:  
```shell
./rottextreme-setup
```
  
Get a DosBox command prompt in the game's folder
to run custom commands:  
```shell
./rottextreme-noop
```
  
Get an IPX networking-enabled DosBox command prompt 
in the game's folder, with instructions on how to
launch NETROTT manually, and access a bunch of neat
DOS-compatible networking utilities you can try 
out for fun:  
```shell
./rottextreme-ipxnoop
```
  
  
More Info:  
----------  
Read the file titled `rise-of-the-triad-info.txt`
for more information about the game and various
options. The content of that file was mostly 
borrowed from someone else, I just added IPX 
Netplay instructions at the top of it, so all
credit for the great info in that file really
goes to the original author (a link to the webpage 
where I downloaded this file is included near the 
top of the file).  
  
  
License:  
--------  
See the comment area at the top of the install.sh
script in this folder for the full license terms
that you must agree to before using any of the 
scripts in this project.  
  
  
===================================================  
  
