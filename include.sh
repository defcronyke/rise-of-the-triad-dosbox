# © 2020 Jeremy Carter <jeremy@jeremycarter.ca>
#
# This is an unofficial helper script written 
# by Jeremy Carter for playing Extreme Rise of 
# the Triad v1.3 from the Steam release of The 
# Apogee Throwback Pack on DosBox running on 
# Linux.
#
# LICENSE: It might break your computer. If it
# does, I'm very sorry about that, but I will
# take no legal responsibility for it. USE AT 
# YOUR OWN RISK, YOU HAVE BEEN WARNED. You can
# modify the files in this project however you 
# want, except that the entire comment area at 
# the top of any files in this project must 
# remain unaltered, other than appending your 
# own name to the copyright, or updating the
# copyright year for your own name.

include_main() {
	echo "info: include.sh file included"
	echo

	EXIT_CODE=0

	if [ -z "$DOXBOX_PATH_DEFAULT" ]; then
                echo "info: env var not set: DOSBOX_PATH_DEFAULT"
                export DOSBOX_PATH_DEFAULT="$HOME/.steam/steam/steamapps/common/The Apogee Throwback Pack/DOSBOX"
                echo "using default value:"
                echo "DOSBOX_PATH_DEFAULT=\"$DOSBOX_PATH_DEFAULT\""
                echo
        fi

        if [ -z "$ROTT_E_DOSBOX_PATH" ]; then
                echo "info: env var not set: ROTT_E_DOSBOX_PATH"
                export ROTT_E_DOSBOX_PATH="$DOSBOX_PATH_DEFAULT"
                echo "using default value:"
                echo "ROTT_E_DOSBOX_PATH=\"$ROTT_E_DOSBOX_PATH\""
                echo
        fi

        if [ ! -d "$ROTT_E_DOSBOX_PATH" ]; then
                echo "error: You need to install The Apogee Throwback Pack from Steam before you can use this. At the time of writing it costs about \$11 CAD, and it comes with two different versions of Rise of the Triad, and two Blake Stone games."
                echo
                echo "link: https://store.steampowered.com/app/238050/The_Apogee_Throwback_Pack/"
                echo
                echo "If you already have it installed but your Steam Library is in an unexpected location, you can change where we look for the game's DOSBOX folder by setting this environment variable: ROTT_E_DOSBOX_PATH"
                echo
                echo "We are currently looking in:"
                echo "ROTT_E_DOSBOX_PATH=\"$ROTT_E_DOSBOX_PATH\""
                echo

				EXIT_CODE=1

                return $EXIT_CODE
        fi

        echo "info: The Apogee Throwback Pack DOSBOX folder location is set as:"
        echo "ROTT_E_DOSBOX_PATH=\"$ROTT_E_DOSBOX_PATH\""
        echo

	which lsb_release
	LSB_CODE=$?

	which wget
	WGET_CODE=$?

	echo

	if [ $LSB_CODE -ne 0 ] || [ $WGET_CODE -ne 0 ]; then
        	echo "info: The lsb_release and/or the wget commands were not found on your system. Attempting to install them (auto-install currently only supports Debian, Arch Linux, and their derivatives. Note that this will also update those packages to the newest versions."

	        (echo "Trying to install with apt-get." && sudo apt-get update && sudo apt-get install lsb-release wget) || (echo "Installing with apt-get failed. Trying to install with pacman." && sudo pacman -Syy && sudo pacman -S lsb-release wget)

        	EXIT_CODE=$?

	        if [ $EXIT_CODE -ne 0 ]; then
        	        echo "error: Failed installing the lsb-release and/or the wget commands. Please install these into your PATH manually before continuing."
			echo
	                return $EXIT_CODE
	        fi
	fi

	export LSB_RELEASE_ID=`lsb_release -is`

	echo "info: Using the command \"lsb_release\", we think your distro is: $LSB_RELEASE_ID"
	echo


	which dosbox
	DOSBOX_CODE=$?

	echo

	if [ $DOSBOX_CODE -ne 0 ]; then
		echo "info: Linux dosbox not found in your PATH. Attempting to install it for you from your distro's official repositories."
		echo

		if [ "$LSB_RELEASE_ID" = "Debian" ]; then
			echo "Trying to install with apt-get."
			sudo apt-get update && sudo apt-get install dosbox
			EXIT_CODE=$?
			echo
		elif [ "$LSB_RELEASE_ID" = "Arch" ]; then
			echo "Trying to install with pacman."
			sudo pacman -Syy && sudo pacman -S dosbox
			EXIT_CODE=$?
			echo
		else
			echo "info: It looks like you aren't running Debian or Arch Linux. We will try installing with apt-get first, and if it fails, we'll try pacman."
			(sudo apt-get update && sudo apt-get install dosbox) || (sudo pacman -Syy && sudo pacman -S dosbox)
			EXIT_CODE=$?
			echo

		fi

		if [ $EXIT_CODE -ne 0 ]; then
			echo "error: Failed installing dosbox. Please install it yourself and make sure it's in your PATH, and then run this install script again."
			echo
				
			return $EXIT_CODE
		fi

	fi

	pwd="$PWD"

	cd ipx
	mkdir -p netutils
	cd netutils


	PKTD11_URL="http://crynwr.com/drivers"
	PKTD11_ARCHIVE_FILENAME="pktd11.zip"

	if [ ! -f ".pktd11-fetched" ]; then
	        echo "info: Fetching: \"$PKTD11_URL/$PKTD11_ARCHIVE_FILENAME\""
	        wget "$PKTD11_URL/$PKTD11_ARCHIVE_FILENAME"
	        touch .pktd11-fetched
	        echo
	else
	        echo "info: Not fetching because it appears you already have this file: \"$PKTD11_URL/$PKTD11_ARCHIVE_FILENAME\""
        	echo "If you'd like to fetch it again anyway, remove these files first and run this script again:"
	        echo
	        echo "    rm \"ipx/$PKTD11_ARCHIVE_FILENAME\" \"ipx/.pktd11-fetched\""
	        echo
	fi

	if [ ! -f ".pktd11-unzipped" ]; then
	        echo "info: Extracting: \"$PKTD11_ARCHIVE_FILENAME\""
        	unzip "$PKTD11_ARCHIVE_FILENAME"
	        touch .pktd11-unzipped
        	echo
	else
        	echo "info: Not extracting because it appears you already extracted this file: \"$PKTD11_ARCHIVE_FILENAME\""
	        echo "If you'd like to extract it again anyway, remove this file first and run this script again:"            
	        echo
	        echo "    rm \"ipx/.pktd11-unzipped\""
	        echo
	fi


	MTCP_URL="https://www.brutman.com/mTCP"
	MTCP_ARCHIVE_FILENAME="mTCP_2020-03-07.zip"

	if [ ! -f ".mtcp-fetched" ]; then
        	echo "info: Fetching: \"$MTCP_URL/$MTCP_ARCHIVE_FILENAME\""
	        wget "$MTCP_URL/$MTCP_ARCHIVE_FILENAME"
	        touch .mtcp-fetched
	        echo                                                                                                          
	else
        	echo "info: Not fetching because it appears you already have this file: \"$MTCP_URL/$MTCP_ARCHIVE_FILENAME\""
	        echo "If you'd like to fetch it again anyway, remove these files first and run this script again:"
	        echo
	        echo "    rm \"ipx/$MTCP_ARCHIVE_FILENAME\" \"ipx/.mtcp-fetched\""
	        echo
	fi

	if [ ! -f ".mtcp-unzipped" ]; then
        	echo "info: Extracting: \"$MTCP_ARCHIVE_FILENAME\""
	        unzip "$MTCP_ARCHIVE_FILENAME"
	        touch .mtcp-unzipped
        	echo
	else
        	echo "info: Not extracting because it appears you already extracted this file: \"$MTCP_ARCHIVE_FILENAME\""
	        echo "If you'd like to extract it again anyway, remove this file first and run this script again:"
	        echo
	        echo "    rm \"ipx/.mtcp-unzipped\""
	        echo
	fi


	if [ ! -f ".netutils-installed" ]; then
		echo "info: Installing DOS-compatible networking utilities into the game's install folder for your convenience: \"$ROTT_E_DOSBOX_PATH/../Extreme Rise of the Triad\""
		cp -r * "$ROTT_E_DOSBOX_PATH/../Extreme Rise of the Triad"
		touch .netutils-installed
		echo
	else
		echo "info: Not installing DOS-compatible networing utilities because it appears you have already installed them. If you'd like to install them again anyway, remove this file first and run this script again:"
		echo
		echo "    rm \"ipx/netutils/.netutils-installed\""
		echo
	fi


	cd "$pwd"

	echo "info: The include.sh file has now finished doing its thing."
	echo
}

include_main

