#!/bin/sh
#
# © 2020 Jeremy Carter <jeremy@jeremycarter.ca>
#
# This is the installer for some unofficial 
# helper scripts written by Jeremy Carter for 
# playing Extreme Rise of the Triad v1.3 from 
# the Steam release of The Apogee Throwback 
# Pack on DosBox running on Linux.
#
# LICENSE: It might break your computer. If it
# does, I'm very sorry about that, but I will
# take no legal responsibility for it. USE AT 
# YOUR OWN RISK, YOU HAVE BEEN WARNED. You can
# modify the files in this project however you 
# want, except that the entire comment area at 
# the top of any files in this project must 
# remain unaltered, other than appending your 
# own name to the copyright, or updating the
# copyright year for your own name.

main() {
	echo "© 2020 Jeremy Carter <jeremy@jeremycarter.ca>"
	echo
	echo "Unofficial helper scripts for playing Extreme Rise of the Triad v1.3 with DoxBox on Linux."
	echo
	echo "See the top of the install.sh file for the license terms you must agree to if you want to run this script."
	echo

	
	pwd="$PWD"

	. "$pwd/include.sh"
	EXIT_CODE=$?

	if [ $EXIT_CODE -ne 0 ]; then
		exit $EXIT_CODE
	fi
	
	
	if [ -z "$ROTT_E_CONF" ]; then
		echo "info: env var not set: ROTT_E_CONF"
		ROTT_E_CONF="ROTT_E.sh"
		echo "using default value:"
		echo "ROTT_E_CONF=\"$ROTT_E_CONF\""
		echo
	fi

	if [ -z "$ROTT_E_HELP_CONF" ]; then
		echo "info: env var not set: ROTT_E_HELP_CONF"
		ROTT_E_HELP_CONF="ROTT_E_HELP.sh"
		echo "using default value:"
		echo "ROTT_E_HELP_CONF=\"$ROTT_E_HELP_CONF\""
		echo
	fi

	if [ -z "$ROTT_E_NOOP_CONF" ]; then
		echo "info: env var not set: ROTT_E_NOOP_CONF"
		ROTT_E_NOOP_CONF="ROTT_E_NOOP.sh"
		echo "using default value:"
		echo "ROTT_E_NOOP_CONF=\"$ROTT_E_NOOP_CONF\""
		echo
	fi

	if [ -z "$ROTT_E_SETUP_CONF" ]; then
		echo "info: env var not set: ROTT_E_SETUP_CONF"
		ROTT_E_SETUP_CONF="ROTT_E_SETUP.sh"
		echo "using default value:"
		echo "ROTT_E_SETUP_CONF=\"$ROTT_E_SETUP_CONF\""
		echo
	fi
	
	if [ ! -f "$ROTT_E_DOSBOX_PATH/ROTT_E.conf.orig" ]; then
		echo "info: First time running the installer, or file not found: ROTT_E.conf.orig"
		echo "creating backup of ROTT_E.conf, which has the file extension .orig added to the end."
		cp "$ROTT_E_DOSBOX_PATH/ROTT_E.conf" "$ROTT_E_DOSBOX_PATH/ROTT_E.conf.orig"
		echo
	fi
	

	echo "info: Installing DosBox config files into the game's DosBox folder:"
	echo "ROTT_E_DOSBOX_PATH=\"$ROTT_E_DOSBOX_PATH\""
	echo

	echo "info: Installing include script: \"$ROTT_E_DOSBOX_PATH/include.sh\""
	cp -f "$pwd/include.sh" "$ROTT_E_DOSBOX_PATH"
	echo


	if [ ! -f "$ROTT_E_DOSBOX_PATH/.$ROTT_E_CONF.sha256sum" ]; then
		echo "info: No sha256sum file found: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_CONF.sha256sum\""
		echo "Computing sha256sum of file: \"$ROTT_E_CONF\""
		echo
		echo "Using command:"
		echo
		echo "    sha256sum \"$ROTT_E_CONF\" | tr -d '\n' | cut -d' ' -f1 > \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_CONF.sha256sum\""
		echo
		ROTT_E_CONF_SHA256SUM="`sha256sum "$ROTT_E_CONF" | tr -d '\n' | cut -d' ' -f1`"
		echo "Computed sha256sum: $ROTT_E_CONF_SHA256SUM"
		echo
		echo "info: Installing file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_CONF\""
        cp "$ROTT_E_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_CONF"
        echo
		echo "$ROTT_E_CONF_SHA256SUM" > "$ROTT_E_DOSBOX_PATH/.$ROTT_E_CONF.sha256sum"
		echo "Checksum saved in file: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_CONF.sha256sum\""
		echo
	else
		echo "Computing sha256sum of file: \"$ROTT_E_CONF\""
        echo
        echo "Using command:"
        echo
        echo "    sha256sum \"$ROTT_E_CONF\" | tr -d '\n' | cut -d' ' -f1"
        echo                                                                    
        ROTT_E_CONF_SHA256SUM="`sha256sum "$ROTT_E_CONF" | tr -d '\n' | cut -d' ' -f1`"
        echo "Computed sha256sum: $ROTT_E_CONF_SHA256SUM"
		ROTT_E_CONF_SHA256SUM_OLD="`cat "$ROTT_E_DOSBOX_PATH/.$ROTT_E_CONF.sha256sum" | tr -d '\n' | cut -d' ' -f1`"
		echo "Previous sha256sum: $ROTT_E_CONF_SHA256SUM_OLD"
		echo

		if [ -f "$ROTT_E_DOSBOX_PATH/$ROTT_E_CONF" ] && [ "$ROTT_E_CONF_SHA256SUM_OLD" = "$ROTT_E_CONF_SHA256SUM" ]; then
			echo "info: Not installing unmodified file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_CONF\""
			echo
		else
			if [ -f "$ROTT_E_DOSBOX_PATH/$ROTT_E_CONF" ]; then
				echo "warning: Your DOSBOX folder already contains the file $ROTT_E_CONF. It will be duplicated with .bak extension added to the end."
				cp "$ROTT_E_DOSBOX_PATH/$ROTT_E_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_CONF.bak"
				echo
			fi
			
			echo "info: Installing file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_CONF\""
			cp "$ROTT_E_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_CONF"
			echo
			echo "$ROTT_E_CONF_SHA256SUM" > "$ROTT_E_DOSBOX_PATH/.$ROTT_E_CONF.sha256sum"
			echo "New checksum saved in file: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_CONF.sha256sum\""
			echo
		fi
	fi


	if [ ! -f "$ROTT_E_DOSBOX_PATH/.$ROTT_E_HELP_CONF.sha256sum" ]; then
		echo "info: No sha256sum file found: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_HELP_CONF.sha256sum\""
		echo "Computing sha256sum of file: \"$ROTT_E_HELP_CONF\""
		echo
		echo "Using command:"
		echo
		echo "    sha256sum \"$ROTT_E_HELP_CONF\" | tr -d '\n' | cut -d' ' -f1 > \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_HELP_CONF.sha256sum\""
		echo
		ROTT_E_HELP_CONF_SHA256SUM="`sha256sum "$ROTT_E_HELP_CONF" | tr -d '\n' | cut -d' ' -f1`"
		echo "Computed sha256sum: $ROTT_E_HELP_CONF_SHA256SUM"
		echo
		echo "info: Installing file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_HELP_CONF\""
        cp "$ROTT_E_HELP_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_HELP_CONF"
        echo
		echo "$ROTT_E_HELP_CONF_SHA256SUM" > "$ROTT_E_DOSBOX_PATH/.$ROTT_E_HELP_CONF.sha256sum"
		echo "Checksum saved in file: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_CONF.sha256sum\""
		echo
	else
		echo "Computing sha256sum of file: \"$ROTT_E_HELP_CONF\""
        echo
        echo "Using command:"
        echo
        echo "    sha256sum \"$ROTT_E_HELP_CONF\" | tr -d '\n' | cut -d' ' -f1"
        echo                                                                    
        ROTT_E_HELP_CONF_SHA256SUM="`sha256sum "$ROTT_E_HELP_CONF" | tr -d '\n' | cut -d' ' -f1`"
        echo "Computed sha256sum: $ROTT_E_HELP_CONF_SHA256SUM"
		ROTT_E_HELP_CONF_SHA256SUM_OLD="`cat "$ROTT_E_DOSBOX_PATH/.$ROTT_E_HELP_CONF.sha256sum" | tr -d '\n' | cut -d' ' -f1`"
		echo "Previous sha256sum: $ROTT_E_HELP_CONF_SHA256SUM_OLD"
		echo

		if [ -f "$ROTT_E_DOSBOX_PATH/$ROTT_E_HELP_CONF" ] && [ "$ROTT_E_HELP_CONF_SHA256SUM_OLD" = "$ROTT_E_HELP_CONF_SHA256SUM" ]; then
			echo "info: Not installing unmodified file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_HELP_CONF\""
			echo
		else
			if [ -f "$ROTT_E_DOSBOX_PATH/$ROTT_E_HELP_CONF" ]; then
				echo "warning: Your DOSBOX folder already contains the file $ROTT_E_HELP_CONF. It will be duplicated with .bak extension added to the end."
				cp "$ROTT_E_DOSBOX_PATH/$ROTT_E_HELP_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_HELP_CONF.bak"
				echo
			fi
			
			echo "info: Installing file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_HELP_CONF\""
			cp "$ROTT_E_HELP_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_HELP_CONF"
			echo
			echo "$ROTT_E_HELP_CONF_SHA256SUM" > "$ROTT_E_DOSBOX_PATH/.$ROTT_E_HELP_CONF.sha256sum"
			echo "New checksum saved in file: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_HELP_CONF.sha256sum\""
			echo
		fi
	fi
		
	
	if [ ! -f "$ROTT_E_DOSBOX_PATH/.$ROTT_E_NOOP_CONF.sha256sum" ]; then
		echo "info: No sha256sum file found: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_NOOP_CONF.sha256sum\""
		echo "Computing sha256sum of file: \"$ROTT_E_NOOP_CONF\""
		echo
		echo "Using command:"
		echo
		echo "    sha256sum \"$ROTT_E_NOOP_CONF\" | tr -d '\n' | cut -d' ' -f1 > \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_NOOP_CONF.sha256sum\""
		echo
		ROTT_E_NOOP_CONF_SHA256SUM="`sha256sum "$ROTT_E_NOOP_CONF" | tr -d '\n' | cut -d' ' -f1`"
		echo "Computed sha256sum: $ROTT_E_NOOP_CONF_SHA256SUM"
		echo
		echo "info: Installing file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_NOOP_CONF\""
        cp "$ROTT_E_NOOP_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_NOOP_CONF"
        echo
		echo "$ROTT_E_NOOP_CONF_SHA256SUM" > "$ROTT_E_DOSBOX_PATH/.$ROTT_E_NOOP_CONF.sha256sum"
		echo "Checksum saved in file: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_NOOP_CONF.sha256sum\""
		echo
	else
		echo "Computing sha256sum of file: \"$ROTT_E_NOOP_CONF\""
        echo
        echo "Using command:"
        echo
        echo "    sha256sum \"$ROTT_E_NOOP_CONF\" | tr -d '\n' | cut -d' ' -f1"
        echo                                                                    
        ROTT_E_NOOP_CONF_SHA256SUM="`sha256sum "$ROTT_E_NOOP_CONF" | tr -d '\n' | cut -d' ' -f1`"
        echo "Computed sha256sum: $ROTT_E_NOOP_CONF_SHA256SUM"
		ROTT_E_NOOP_CONF_SHA256SUM_OLD="`cat "$ROTT_E_DOSBOX_PATH/.$ROTT_E_NOOP_CONF.sha256sum" | tr -d '\n' | cut -d' ' -f1`"
		echo "Previous sha256sum: $ROTT_E_NOOP_CONF_SHA256SUM_OLD"
		echo

		if [ -f "$ROTT_E_DOSBOX_PATH/$ROTT_E_NOOP_CONF" ] && [ "$ROTT_E_NOOP_CONF_SHA256SUM_OLD" = "$ROTT_E_NOOP_CONF_SHA256SUM" ]; then
			echo "info: Not installing unmodified file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_NOOP_CONF\""
			echo
		else
			if [ -f "$ROTT_E_DOSBOX_PATH/$ROTT_E_NOOP_CONF" ]; then
				echo "warning: Your DOSBOX folder already contains the file $ROTT_E_NOOP_CONF. It will be duplicated with .bak extension added to the end."
				cp "$ROTT_E_DOSBOX_PATH/$ROTT_E_NOOP_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_NOOP_CONF.bak"
				echo
			fi
			
			echo "info: Installing file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_NOOP_CONF\""
			cp "$ROTT_E_NOOP_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_NOOP_CONF"
			echo
			echo "$ROTT_E_NOOP_CONF_SHA256SUM" > "$ROTT_E_DOSBOX_PATH/.$ROTT_E_NOOP_CONF.sha256sum"
			echo "New checksum saved in file: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_NOOP_CONF.sha256sum\""
			echo
		fi
	fi
	
	
	if [ ! -f "$ROTT_E_DOSBOX_PATH/.$ROTT_E_SETUP_CONF.sha256sum" ]; then
		echo "info: No sha256sum file found: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_SETUP_CONF.sha256sum\""
		echo "Computing sha256sum of file: \"$ROTT_E_SETUP_CONF\""
		echo
		echo "Using command:"
		echo
		echo "    sha256sum \"$ROTT_E_SETUP_CONF\" | tr -d '\n' | cut -d' ' -f1 > \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_SETUP_CONF.sha256sum\""
		echo
		ROTT_E_SETUP_CONF_SHA256SUM="`sha256sum "$ROTT_E_SETUP_CONF" | tr -d '\n' | cut -d' ' -f1`"
		echo "Computed sha256sum: $ROTT_E_SETUP_CONF_SHA256SUM"
		echo
		echo "info: Installing file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_SETUP_CONF\""
        cp "$ROTT_E_SETUP_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_SETUP_CONF"
        echo
		echo "$ROTT_E_SETUP_CONF_SHA256SUM" > "$ROTT_E_DOSBOX_PATH/.$ROTT_E_SETUP_CONF.sha256sum"
		echo "Checksum saved in file: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_SETUP_CONF.sha256sum\""
		echo
	else
		echo "Computing sha256sum of file: \"$ROTT_E_SETUP_CONF\""
        echo
        echo "Using command:"
        echo
        echo "    sha256sum \"$ROTT_E_SETUP_CONF\" | tr -d '\n' | cut -d' ' -f1"
        echo                                                                    
        ROTT_E_SETUP_CONF_SHA256SUM="`sha256sum "$ROTT_E_SETUP_CONF" | tr -d '\n' | cut -d' ' -f1`"
        echo "Computed sha256sum: $ROTT_E_SETUP_CONF_SHA256SUM"
		ROTT_E_SETUP_CONF_SHA256SUM_OLD="`cat "$ROTT_E_DOSBOX_PATH/.$ROTT_E_SETUP_CONF.sha256sum" | tr -d '\n' | cut -d' ' -f1`"
		echo "Previous sha256sum: $ROTT_E_SETUP_CONF_SHA256SUM_OLD"
		echo

		if [ -f "$ROTT_E_DOSBOX_PATH/$ROTT_E_SETUP_CONF" ] && [ "$ROTT_E_SETUP_CONF_SHA256SUM_OLD" = "$ROTT_E_SETUP_CONF_SHA256SUM" ]; then
			echo "info: Not installing unmodified file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_SETUP_CONF\""
			echo
		else
			if [ -f "$ROTT_E_DOSBOX_PATH/$ROTT_E_SETUP_CONF" ]; then
				echo "warning: Your DOSBOX folder already contains the file $ROTT_E_SETUP_CONF. It will be duplicated with .bak extension added to the end."
				cp "$ROTT_E_DOSBOX_PATH/$ROTT_E_SETUP_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_SETUP_CONF.bak"
				echo
			fi
			
			echo "info: Installing file: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_SETUP_CONF\""
			cp "$ROTT_E_SETUP_CONF" "$ROTT_E_DOSBOX_PATH/$ROTT_E_SETUP_CONF"
			echo
			echo "$ROTT_E_SETUP_CONF_SHA256SUM" > "$ROTT_E_DOSBOX_PATH/.$ROTT_E_SETUP_CONF.sha256sum"
			echo "New checksum saved in file: \"$ROTT_E_DOSBOX_PATH/.$ROTT_E_SETUP_CONF.sha256sum\""
			echo
		fi
	fi
	

	ROTT_E_IPX_DIR="ipx"

	echo "info: Installing directory: \"$ROTT_E_DOSBOX_PATH/$ROTT_E_IPX_DIR\""
	mkdir -p "$ROTT_E_DOSBOX_PATH/$ROTT_E_IPX_DIR"
	#cp -ur "$ROTT_E_IPX_DIR" "$ROTT_E_DOSBOX_PATH/$ROTT_E_IPX_DIR"
	yes | cp -rf "$pwd/$ROTT_E_IPX_DIR"/* "$ROTT_E_DOSBOX_PATH/$ROTT_E_IPX_DIR/"
	echo

	#pwd=`dirname "$0"`
	pwd="$PWD"

	cd "$ROTT_E_DOSBOX_PATH/../Extreme Rise of the Triad"

	if [ ! -f ".extreme-unzipped" ]; then
		echo "info: Unzipping extra levels into game install directory: EXTREME.zip."
		unzip EXTREME.zip
		touch .extreme-unzipped
		echo
	else
		echo "info: Not unzipping extra levels: EXTREME.zip"
		echo "It appears you already unzipped them during a previous run of this install script. If you want to unzip them again anyway, remove the following file and run the install script again:"
		echo
		echo "    rm \"$ROTT_E_DOSBOX_PATH/../Extreme Rise of the Triad/.extreme-unzipped\""
		echo
	fi

	if [ ! -f ".morelvls-unzipped" ]; then
		echo "info: Unzipping extra levels into game install directory: MORELVLS.ZIP"
		unzip MORELVLS.ZIP
		touch .morelvls-unzipped
		echo
	else
		echo "info: Not unzipping extra levels: MORELVLS.ZIP"
		echo "It appears you already unzipped them during a previous run of this install script. If you want to unzip them again anyway, remove the following file and run the install script again:"
		echo
		echo "    rm \"$ROTT_E_DOSBOX_PATH/../Extreme Rise of the Triad/.morelvls-unzipped\""
		echo
	fi

	cd ..

	if [ ! -f ".rott_soundtrack-unzipped" ]; then
		echo "info: Unzipping soundtrack into the parent folder of the game install directory: rott_soundtrack.zip"
		mkdir -p rott_soundtrack
		cd rott_soundtrack
		unzip ../rott_soundtrack.zip
		cd ..
		touch .rott_soundtrack-unzipped
		echo
	else
		echo "info: Not unzipping soundtrack: rott_soundtrack.zip"
		echo "It appears you already unzipped it during a previous run of this install script. If you want to unzip it again anyway, remove the following file and run the install script again:"
		echo
		echo "    rm \"$ROTT_E_DOSBOX_PATH/../.rott_soundtrack-unzipped\""
		echo
	fi
	
	
	cd "$pwd"

	echo "info: Installation complete. Enjoy!"
	echo

	return 0
}

main
exit $?

