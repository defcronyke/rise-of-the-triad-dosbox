#!/bin/sh
#
# © 2020 Jeremy Carter <jeremy@jeremycarter.ca>
#
# This is an unofficial helper script written 
# by Jeremy Carter for playing Extreme Rise of 
# the Triad v1.3 from the Steam release of The 
# Apogee Throwback Pack on DosBox running on 
# Linux.
#
# LICENSE: It might break your computer. If it
# does, I'm very sorry about that, but I will
# take no legal responsibility for it. USE AT 
# YOUR OWN RISK, YOU HAVE BEEN WARNED. You can
# modify the files in this project however you 
# want, except that the entire comment area at 
# the top of any files in this project must 
# remain unaltered, other than appending your 
# own name to the copyright, or updating the
# copyright year for your own name.

pwd="$PWD"

. "./include.sh"
EXIT_CODE=$?

if [ $EXIT_CODE -ne 0 ]; then
    exit $EXIT_CODE
fi


if [ -z "$SERVER_ADDR"]; then
	echo "warning: env var not set: SERVER_ADDR"
	SERVER_ADDR="192.168.1.155"
	echo "Using default value (THIS IS PROBABLY NOT THE SERVER YOU WANT. YOU SHOULD OVERRIDE THIS TO POINT AT THE SERVER YOU WANT TO CONNECT TO): $SERVER_ADDR"
	echo
fi

if [ -z "$HOSTNAME" ]; then
        echo "info: env var not set: HOSTNAME"
        HOSTNAME=`hostname -s`
        echo "Using default value:"
        echo "HOSTNAME=\"$HOSTNAME\""
        echo
fi

if [ -z "$SERVER_PORT" ]; then
	echo "info: env var not set: SERVER_PORT"
	SERVER_PORT="1088"
	echo "Using default value:"
	echo "SERVER_PORT=\"$SERVER_PORT\""
	echo
fi

LSB_DISTRIB_ID=`lsb_release -is`

if [ "$LSB_DISTRIB_ID" = "Arch" ]; then
	if [ -z "$CLIENT_ADDR" ]; then
                echo "info: env var not set: CLIENT_ADDR"
                CLIENT_ADDR=`hostname -i | awk '{print $NF}'`
                echo "Using default value:"
                echo "CLIENT_ADDR=\"$CLIENT_ADDR\""
                echo
        fi

        GAME_DIR_NUM="2"
	echo "GAME_DIR_NUM=\"$GAME_DIR_NUM\""
	echo
	
	if [ `hostname -s` = "piano" ]; then
		MIDI_CONFIG=`aplaymidi -l | head -n 3 | tail -n 1 | awk '{print $1}'` # 28:0
		echo "We think you are Jeremy (the author if these scripts), due to your hostname. Using the required midi device ID for Jeremy's non-standard Arch Linux configuration:"
		echo "MIDI_CONFIG=\"$MIDI_CONFIG\""
		echo
	else
		MIDI_CONFIG=`aplaymidi -l | head -n 2 | tail -n 1 | awk '{print $1}'` # 14:0
		echo "MIDI_CONFIG=\"$MIDI_CONFIG\""
                echo
	fi

elif [ "$LSB_DISTRIB_ID" = "Kali" ]; then
        if [ -z "$CLIENT_ADDR" ]; then
                echo "info: env var not set: CLIENT_ADDR"
		# TODO: Is this the correct hostname command invocation for Kali?
                CLIENT_ADDR=`hostname -I | awk '{print $1}'`
                echo "Using default value:"
                echo "CLIENT_ADDR=\"$CLIENT_ADDR\""
                echo
        fi

        GAME_DIR_NUM="2"
	echo "GAME_DIR_NUM=\"$GAME_DIR_NUM\""
        echo

        MIDI_CONFIG=`aplaymidi -l | head -n 2 | tail -n 1 | awk '{print $1}'` # 14:0
	echo "MIDI_CONFIG=\"$MIDI_CONFIG\""
        echo
else
	if [ -z "$CLIENT_ADDR" ]; then
                echo "info: env var not set: CLIENT_ADDR"
                CLIENT_ADDR=`hostname -I | awk '{print $1}'`
                echo "Using default value:"
                echo "CLIENT_ADDR=\"$CLIENT_ADDR\""
                echo
        fi

        GAME_DIR_NUM="1"
	echo "GAME_DIR_NUM=\"$GAME_DIR_NUM\""
        echo

	MIDI_CONFIG=`aplaymidi -l | head -n 2 | tail -n 1 | awk '{print $1}'` # 14:0
	echo "MIDI_CONFIG=\"$MIDI_CONFIG\""
        echo
fi

if [ ! -z "$CLIENT_SERVER" ]; then
	echo "info: Detected client and server running on the same host. Setting IP addresses correctly for that."
	SERVER_ADDR="$CLIENT_ADDR"
	echo "SERVER_ADDR=\"$SERVER_ADDR\""
	echo
fi

echo "BASENAME=\"$BASENAME\""
echo

DEFAULT_GAME_CMD="ROTT.EXE"
echo "DEFAULT_GAME_CMD=\"$DEFAULT_GAME_CMD\""
echo

if [ "$BASENAME" = "rottextreme-ipx" ]; then
    GAME_CMD="SETUP.EXE"
    echo "You are the master player, so we'll run \"$GAME_CMD\" instead of \"$DEFAULT_GAME_CMD\" so you can make any changes you want to the game config before launching. For example, you might want to activate one of the non-standard level packs found in the \"Use Modified Stuff\" section of the setup menu."
    echo
    echo "GAME_CMD=\"$GAME_CMD\""
    echo
elif [ "$BASENAME" = "rottextreme-ipxclient" ]; then
	GAME_CMD="SETUP.EXE"
	echo "GAME_CMD=\"$GAME_CMD\""
	echo
else
    GAME_CMD="$DEFAULT_GAME_CMD"
    echo "GAME_CMD=\"$GAME_CMD\""
    echo
fi

cat <<EOF | sed "s/{{GAME_CMD}}/$GAME_CMD/" | sed "s/{{GAME_DIR_NUM}}/$GAME_DIR_NUM/" | sed "s/{{MIDI_CONFIG}}/$MIDI_CONFIG/" | sed "s/{{HOSTNAME}}/$HOSTNAME/" | sed "s/{{CLIENT_ADDR}}/$CLIENT_ADDR/" | sed "s/{{SERVER_ADDR}}/$SERVER_ADDR/" | sed "s/{{SERVER_PORT}}/$SERVER_PORT/" | tee game-dos.cnf
[sdl]
fullscreen=False
fulldouble=false
fullresolution=1920x1080
windowresolution=1280x1024
output=opengl
autolock=true
sensitivity=100
waitonerror=false
priority=higher,normal
mapperfile=mapper-0.74.map
usescancodes=true

[dosbox]
language=
machine=svga_s3
captures=.\Captures\
memsize=16

[render]
frameskip=0
aspect=true
scalar=normal3x

[cpu]
core=auto
cputype=auto
cycles=fixed 50000
cycleup=10
cycledown=20

[mixer]
nosound=false
rate=22050
blocksize=2048
prebuffer=10

[midi]
mpu401=intelligent
mididevice=alsa
midiconfig={{MIDI_CONFIG}}

[sblaster]
sbtype=sb16
sbbase=220
irq=7
dma=1
hdma=5
sbmixer=true
oplmode=auto
oplemu=default
oplrate=44100

[gus]
ultradir=C:\
gusrate=44100
gusbase=240
gusirq=5
gusdma=3
gus=false

[speaker]
pcspeaker=true
pcrate=44100
tandy=auto
tandyrate=44100
disney=true

[joystick]
joysticktype=none
timed=true
autofire=false
swap34=false
buttonwrap=false

[serial]
serial1=dummy
serial2=dummy
serial3=disabled
serial4=disabled

[dos]
xms=true
ems=true
umb=true
keyboardlayout=auto

[ipx]
ipx=true

[autoexec]
echo Starting game.
mount c ".."
c:
cd EXTREM~{{GAME_DIR_NUM}}

{{GAME_CMD}}

EOF

sleep 5
dosbox -conf game-dos.cnf
exit $?

